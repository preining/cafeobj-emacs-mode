;;; cafeobj-mode.el --- CafeOBJ code editing and Interaction with
;;;                     CafeOBJ interpreter.
;;; Copyleft: verbatim copies of this file may be freely redistributed.
;;; This file is distributed `as is', without warranties of any kind.
;;;
;;;                                   Toshimi Sawada <sawada@sra.co.jp>
;;;                              
;;; $Id: cafeobj-mode.el,v 1.3 1998/07/06 08:25:29 sawada Exp $
;;; revised by Min Zhang 2011/11/20
;;; Keywords: cafeobj chaos languages processes tools
;;;
;;; BRIEF DESCRIPTION:
;;; CafeOBJ-mode supports formatting CafeOBJ source codes and sending of
;;; declarations/regions/files to a CafeOBJ interpreter. An interpreter 
;;; (see variable `cafeobj-default-application') will be started if you try
;;; to send some code and none is running. You can use the process-buffer
;;; (named after the application you chose) as if it were an
;;; interactive shell. See the documentation for `comint.el' for
;;; details.
;;; *NOTE* : This mode supports Chaos with CafeOBJ syntax only.

;;; ====== DESCRIPTION =========================================================
;;; 
;;; USAGE:
;;; The following statements, placed in your .emacs file or
;;; site-init.el, will cause this file to be autoloaded, and
;;; cafeobj-mode invoked, when visiting .mod files (assuming this file is
;;; in your load-path):
;;;
;;;	(autoload 'cafeobj-mode "cafeobj-mode" "CafeOBJ mode." t)
;;;	(setq auto-mode-alist
;;;	      (cons '("\\.mod$" . cafeobj-mode) auto-mode-alist))
;;;
;;; and, also the following is handy for running CafeOBJ interpreter:
;;;
;;;     (autoload 'cafeobj "cafeobj-mode" "Run CafeOBJ interpreter." t)
;;;
;;;
;;; KEY BINDINGS:
;;; To see all the keybindings for folding mode, look at `cafeobj-setup-keymap'
;;; or start `cafeobj-mode' and type `\C-h m'.
;;; The keybindings are defined with prefixed by 'cafeobj-prefix-key',
;;; the default for `cafeobj-prefix-key' is `\C-c', which is the conventional
;;; prefix for major-mode commands.
;;; You can customise the keybindings either by setting `choas-prefix-key'
;;; or by putting the following in your .emacs
;;; 	(setq cafeobj-mode-map (make-sparse-keymap))
;;; and
;;; 	(define-key cafeobj-mode-map <your-key> <function>)
;;; for all the functions you need.
;;;
;;; ABBRIVE-MODE:
;;; CafeOBJ mode provides a mode-specific abbrev table 'cafeobj-mode-abbrev-table',
;;; and there defined some abbriviations already.
;;; You can use abbrev by `M-x abbrev-mode' or putting the following in your
;;; .emacs
;;;    (add-hook 'cafeobj-mode-hook (function (lambda () (abbrev-mode 1))))
;;;
;;; The predefined abbribiations are minimum, these are derived from my own
;;; experience. I want these definitions to be useful for most of the users,
;;; but ofcourse, you may feel it does not match to your taste. In that case,
;;; add your own definitions for preventing my deinitions.
;;; To edit the entire table, `M-x edit-abbrevs' and `M-x write-abbrev-file'
;;; are handy. Here is an example of adding some extra abbreviations:
;;; I want the following abbreviations:
;;;     "mod"	->  "module"
;;;     "mod*"  ->  "module*"
;;;     "mod!"  ->  "module!"
;;;     "obj"	->  "module!"
;;;     "th"	->  "module*"
;;; For this, we can use `M-x edit-abbrevs' and adds the above definitions in
;;; cafeobj-abbrev-table like this:
;;;
;;; (cafeobj-mode-abbrev-table)
;;;
;;;    "mod"	 0     "module"
;;;    "m*"	 0     "module* "
;;;    "m!"      0     "module! "         
;;;    "obj"     0     "module!"
;;;    "th"      0     "module*"
;;;    "comm"    0     "commutative"
;;;            :
;;;            :
;;; After editting, type `C-c C-c'(M-x edit-abbrevs-redefine) to install
;;; abbrev definitions as specified. 
;;; Then use `M-x write-abbrev-file' specifying ~/.abbrev_deffs to
;;; the target file name, and adds a line in my .emacs
;;; (read-abbrev-file "~/.abbrev_defs").
;;; See Emacs manual of Infos for detail.
;;;
;;; CUTOMIZABLE VARIABLES:
;;; You may want to customize the following variables, see the comment strings
;;; for the descriptions of these variables.
;;;
;;; 	cafeobj-always-show
;;;	cafeobj-mode-map
;;;	cafeobj-prefix-key
;;;	cafeobj-mode-hook
;;; 	cafeobj-default-application
;;; 	cafeobj-default-command-switches
;;;     cafeobj-decl-keyword-face
;;;     cafeobj-keyword-face
;;;     cafeobj-command-face
;;;     cafeobj-comment-face
;;;     cafeobj-indent-level
;;;     cafeobj-brace-imaginary-offset
;;;     cafeobj-brace-offset
;;;     cafeobj-psort-indent
;;;     cafeobj-continued-statement-offset
;;;     cafeobj-continued-brace-offset
;;;
;;; see document strings for some brief description of each variable.
;;;

(require 'font-lock)
(require 'comint)
(require 'compile)
(require 'derived)
(require 'ansi-color)
(require 'derived)
(require 'easymenu)
(require 'imenu)


;;; -----------------------
;;; Customizable variables _____________________________________________________
;;; -----------------------

(defvar cafeobj-always-show t
  "*Non-nil means display cafeobj-process-buffer after sending a command.")

;; (defvar cafeobj-mode-map nil
;;   "Keymap used with chaos mode.")

(defvar cafeobj-prefix-key "\C-c"
  "Prefix for all cafeobj-mode commands.")

(defvar cafeobj-mode-hook nil
  "Hooks called when chaos mode fires up.")

(defvar cafeobj-default-application "cafeobj"
  "Default Chaos dialect to run in chaos subprocess.")

(defvar cafeobj-default-command-switches nil
  "Command switches for `cafeobj-default-application'.
Should be a list of strings which will be given to the cafeobj process when star up.")

;;; below, we define `faces' of various CafeOBJ syntactic categories.
;;; these are used in font-lock mode for displaying CafeOBJ sources.
;;; You may wish to change these values to suit your taste; set them
;;; in your .emacs or elsewhere which is evaluated BEFORE this file.

;; Making faces
(defface cafeobj-attribute-face nil
  "attributes to operators (like comm and gather)"
  :group 'cafeobj)
(defvar cafeobj-attribute-face 'cafeobj-attribute-face
  "Face for attributes to operators (like comm and gather)")

(defface cafeobj-attribute-value-face nil
  "values of attributes to operators (like comm and gather)"
  :group 'cafeobj)
(defvar cafeobj-attribute-value-face 'cafeobj-attribute-value-face
  "Face for values of attributes to operators (like comm and gather)")

(defface cafeobj-element-face nil
  "about what values we can have in a sort (ctor, subsort etc)"
  :group 'cafeobj)
(defvar cafeobj-element-face 'cafeobj-element-face
  "Face for about what values we can have in a sort (ctor, subsort etc)")

(defface cafeobj-start-face
  '((t (:inherit font-lock-variable-name-face :weight bold)))
  "Face to starting words like fmod in cafeobj"
  :group 'cafeobj)
(defvar cafeobj-start-face 'cafeobj-start-face
  "Face to starting words like fmod in cafeobj")

(defface cafeobj-module-name-face
  '((t (:inherit font-lock-type-face :weight bold)))
  "Face to declaration of e.g. modules in cafeobj"
  :group 'cafeobj)
(defvar cafeobj-module-name-face 'cafeobj-module-name-face
  "Face to declaration of e.g. modules in cafeobj")

(defface cafeobj-pattern-face
  '((t (:slant italic)))
  "Face in patterns in ifs and equations"
  :group 'cafeobj)
(defvar cafeobj-pattern-face 'cafeobj-pattern-face
  "Face in patterns in ifs and equations")

(defface cafeobj-label-face
  '((((type x w32 mac) (class color))
     (:box (:line-width -1 :style released-button)
           ;; 	   :background "grey75" :foreground "black"
           ))
    (t
     (:inverse-video t)))
  "Face on labels in cafeobj."
  :group 'cafeobj)
(defvar cafeobj-label-face 'cafeobj-label-face
  "Face on labels in cafeobj.")

(defface cafeobj-file-face
  '((t (:inherit cafeobj-module-name-face :inverse-video t)))
  "Face on files and directories"
  :group 'cafeobj)
(defvar cafeobj-file-face 'cafeobj-file-face
  "Face on files and directories")

(defface cafeobj-comment-highlight-face
  '((t (:inherit font-lock-comment-face :weight bold)))
  "Face on 'comment headlines' with four asterisks"
  :group 'cafeobj)
(defvar cafeobj-comment-highlight-face 'cafeobj-comment-highlight-face
  "Face on 'comment headlines' with four asterisks")

(defface cafeobj-comment-highlight-highlight-face
  '((t (:inherit cafeobj-comment-highlight-face :inverse-video t)))
  "Face on important 'comment headlines' with five asterisks"
  :group 'cafeobj)

(defface cafeobj-end-face
  '((t (:inherit bold)))
  "Face on the final '.'"
  :group 'cafeobj)
(defvar cafeobj-end-face 'cafeobj-end-face
  "Face on the final '.'")

 (defvar cafeobj-decl-keyword-face font-lock-keyword-face
   "a face used for top level declaration keywords, such as module, view.")
 (defvar cafeobj-keyword-face font-lock-function-name-face
   "a face used for displaying keywords of module constructs, ex. op, eq ..")
 (defvar cafeobj-command-face font-lock-keyword-face
   "a face used for displaying top-level commands, show, describe, open, and so on.")
 (defvar cafeobj-comment-face font-lock-comment-face
   "a face for comment lines.")

;;; INDENTATION CONTROL

(defvar cafeobj-indent-level 2
  "*Indentation of CafeOBJ statements with respect to containing block.")
(defvar cafeobj-brace-imaginary-offset 0
  "*Imagined indentation of a CafeOBJ open brace that actually follows a statement.")
(defvar cafeobj-brace-offset 0
  "*Extra indentation for braces, compared with other text in same context.")
(defvar cafeobj-psort-indent 5
  "*Indentation level of principal-sort declaration of CafeOBJ.")
(defvar cafeobj-continued-statement-offset 2
  "*Extra indent for lines not starting new statements.")
(defvar cafeobj-continued-brace-offset 0
  "*Extra indent for substatements that start with open-braces.
This is in addition to cafeobj-continued-statement-offset.")

;;; ---------------------------------
;;; globals for internal use only....
;;; ---------------------------------

(defvar cafeobj-process nil
  "The active chaos subprocess corresponding to current buffer.")

(defvar cafeobj-process-buffer nil
  "Buffer used for communication with chaos subprocess for current buffer.")

(defvar cafeobj-region-start (make-marker)
  "Start of special region for chaos communication.")

(defvar cafeobj-region-end (make-marker)
  "End of special region for chaos communication.")


;;;---------------------------
;;; CafeOBJ Keywords/Commands __________________________________________________
;;; essential syntactic structures are defined here.
;;;---------------------------

(defconst cafeobj-comment-pat "\\(--\\|\\*\\*\\)[ \t\n]+")

(defconst cafeobj-keywords
    '("op" "ops" "\\[" "\\]" "\\*\\[" "\\]\\*"
      "bop" "pred" "bops"
      "bpred"
      "imports"
      "imports{*"
      "protecting" "pr"
      "including" "inc"
      "including(*" "inc(*"
      "protecting(*" "pr(*"
      "extending" "ex" "using" "us"
      "extending(*" "ex(*" "using(*" "us(*"
      "signature" "axioms" 
      "signature{*" "axioms{*" 
      "var" "vars"
      "eq" "cq" "ceq" 
      "bq" "beq" "bcq" "bceq" 
      "trans" "trns" "ctrans" "ctrns" "comm" "assoc" "constr"
      "btrans" "btrns" "bctrans" "bctrns"
      "class" "record" "attr"
      "view"
      "open" 
      "close"
      "sort" "hsort"
      "psort" "principal-sort")
  "keywords appearing inside module declaration or view declaration."
  )

(defconst cafeobj-keyword-pat
    (mapconcat 'identity cafeobj-keywords "\\|"))

(defun looking-at-cafeobj-keyword-pat ()
  (looking-at cafeobj-keyword-pat))

(defconst cafeobj-commands
    '("let" "red" "reduce" "input" "in" 
      "execute" "exec" "make"
      "apply" "start" "match"
      "parse"
      "select"
      "set"
      "show"
      "describe"
      "sh"
      "tram"
      "choose" "open" "close"
      "desc" "eof"
      "require" "provide")
  "CafeOBJ top-level commands")

(defconst cafeobj-command-pat
    (mapconcat 'identity cafeobj-commands "\\|"))

(defun looking-at-cafeobj-command-pat ()
  (looking-at cafeobj-command-pat))

(defconst cafeobj-top-keywords
    '("op" "ops" "bop" "bops" "pred" 
      "var" "vars"
      "eq" "cq" "ceq" 
      "bq" "beq" "bcq" "bceq" 
      "trans" "trns" "ctrans" "ctrns" 
      "btrans" "btrns" "bctrans" "bctrns"
      "class" "record" "attr" "open" "close"
      "\\[" "\\]")
  "CafeOBJ keyowrds may apper at top-level.")

(defconst cafeobj-warnings
  (concat "\\(\\<eof\\>"                ; eof
          "\\|\\<quit\\>\\|^\\s-*?q\\>" ; quit
          "\\|\\w\\.\\s-*?$"            ; text.
          "\\|\\]\\."                   ; ].
          "\\|)\\."                     ; ).
          "\\|\\w->"                    ; text->
          "\\|->\\w"                    ; ->text
          "\\|\\w=>"                    ; text=>
          "\\|=>\\w"                    ; =>text
          "\\|var.*\\w:"                ; var text:
          "\\|var.*:\\w"                ; var :text
          "\\|\\<eq\\>.*\\s-=>\\s-"     ; eq foo => bar
          "\\|\\<rl\\>.*\\s-=[ ][^\n][^\n]" ; rl foo = bar ; This doesn't work
          "\\|\\<var\\>\\s-+\\w+\\s-+\\w+"  ; var, not vars
          "\\|\\<sort\\>\\s-+\\w+\\s-+[a-su-zA-Z][a-np-zA-Z]\\w*?" ; sort, not sorts.  Not sort Foo to Bar .
          "\\|\\<op\\>\\s-+\\w+\\s-+\\w+.*:" ; op, not ops
          "\\|\\<vars?\\>.*?,"               ; vars foo , bar
          "\\|\\<sorts?\\>.*?,"              ; sorts foo , bar
          "\\|\\<ops?\\>[^:$_t(]*?," ; ops foo , bar  The reason for the t is mappings in full maude: op identity to vector(0,0) .
          "\\|\\<prec\\w*\\>\\s-*9[6-9]" ; I have no Idea why this caused error once.  Precedence < 95 ok
          "\\|\\<prec\\w*\\>\\s-*1[1-3][0-9]" ; I have no Idea why this caused error once.  Precedence < 95 ok
          "\\|\\<prec\\w*\\>\\s-*[2-9][0-9][0-9]" ; Illegal high precedences?
          "\\|^omod" ; Should have full maude here, with "(" before
          "\\)")
  "Regexps to be coloured with warning-face.")

(defun looking-at-cafeobj-top-keywords ()
  (looking-at cafeobj-top-keywords))

(defconst cafeobj-top-key-pat
    (mapconcat 'identity cafeobj-top-keywords "\\|"))

(defconst cafeobj-top-decl-pat 
    "^\\(module\\|mod\\|module\*\\|mod\*\\|module!\\|mod!\\|view\\)")

(defun looking-at-cafeobj-top-decl ()
  (looking-at "\\(module\\|mod\\|module\*\\|mod\*\\|module!\\|mod!\\|view\\)")
  )

(defconst cafeobj-block-start-pat
    "\\(signature\\|axioms\\|imports\\|record\\|class\\)")

(defun looking-at-cafeobj-block-start-pat ()
  (looking-at "\\(signature\\|axioms\\|imports\\|record\\|class\\)"))
       
(defun looking-at-cafeobj-module-decl ()
  (looking-at "module\\|mod\\|module\*\\|mod\*\\|module!\\|mod!"))

(defun looking-at-cafeobj-view-decl ()
  (looking-at "view"))

;;;----------
;;; FONT-LOCK___________________________________________________________________
;;;----------

(defun get-cafeobj-face (name)
  (cond (cafeobj-xemacs-p (symbol-value name))
	(t (identity name))))

;; Temporary variables for cafeobj-font-lock-regexp.  
;; However, didnt find elegant way of setting them local (with let or something)
;; These regexps match the space after them.
(defvar cafeobj-flk-label "\\(\\(\\[.+?\\]\\)\\s-+?\\(:\\)\\s-+?\\)?") ; [label]
(defvar cafeobj-flk-pattern "\\(.*?\\)\\s-+?")              ; pattern term
(defvar cafeobj-flk-term "\\(.+\\)\\s-+")                   ; term
(defvar cafeobj-flk-name "\\(\\w+\\)\\s-+") ; General name.  Try to use sth else
(defvar cafeobj-flk-type-name "\\([a-zA-Z0-9()|{},<>$-]+@?[a-zA-Z0-9()|{},<>$-]*\\s-+\\)") ; sort name.  May contain @{}-,<>$ and several ()
      ;; (defvar cafeobj-flk-module "\\(\\w\\S-*\\s-+\\)") ; module name	
(defvar cafeobj-flk-mod-id "\\(\\w\\S-*\\s-+\\)") ; module name	
(defvar cafeobj-flk-mod-exp "\\(\\w.*?\\)\\s-+") ; module expression.  May be parametrised module, M*N, M+N, (M)
(defvar cafeobj-flk-end "\\s-?\\(\\.\\)")         ; end of command. XXX make whitespace mandatory once this isn't used in other expressions
(defvar cafeobj-flk-end-command "\\(\\.\\))?\\s-") ; end of command.  ) 
(defvar cafeobj-flk-number-in-square "\\(\\[[0-9]+\\]\\s-+\\)?") ; [10]
(defvar cafeobj-flk-in-module "\\(\\(\\<in\\>\\)\\s-+\\(\\w+\\)\\s-+\\)?") ; in FOO : 
(defvar cafeobj-flk-term-possibly-two-lines  ".*?\\s-*?.*?\\s-*?")
(defvar cafeobj-flk-debug "\\(\\<debug\\>\\s-+\\)?")
(defvar cafeobj-flk-such-that-condition "\\(\\(\\<such\\s-+that\\>\\|\\<s\\.t\\.\\)\\s-+\\(.+\\)\\s-\\)?")
(defvar cafeobj-flk-file-name "\\(\\S-+\\)\\s-*")
(defvar cafeobj-flk-directory "\\(\\w\\S-*\\)\\s-*")
(defvar cafeobj-flk-on-off "\\<\\(on\\|off\\)\\>\\s-+")

(defun cafeobj-flk-keyword (keyword)
  (concat "\\(\\<" keyword "\\>\\)\\s-+?"))
(defun cafeobj-flk-attribute (attribute)
  (concat "\\[.*\\(\\<" attribute "\\>\\).*]"))
(defun cafeobj-flk-attribute-value (attribute value)
  (concat "\\[.*\\(\\<" attribute "\\>\\)\\s-+\\(" value "\\).*]"))
(defun cafeobj-flk-attribute-colon-value (attribute value)
  (concat "\\[.*\\(\\<" attribute ":\\)\\s-+\\(" value "\\).*]"))

;; To a certain degree, this follows the order of the cafeobj grammar
(defconst cafeobj-font-lock-keywords
  (list
   ;; Fontify keywords
   ;;   (cons  (concat "\\<\\(" cafeobj-keywords "\\)\\>") 'font-lock-keyword-face)
   ;;    ;; Fontify system
   ;;    (cons (concat "\\<\\(" system "\\)\\>") 'font-lock-type-face)
   ;; punctuations : . ->    =    =>     <=    <      >    =/=
   ;;	 (cons "\:\\|\\.\\|\-\>\\|\~\>\\|\=\\|\=\>\\|\<\=\\|\<\\|\>\\|\\/" 'font-lock-keyword-face)
;;; SYSTEM COMMANDS
   (list (concat "^\\s-*" (cafeobj-flk-keyword "in") cafeobj-flk-file-name "$")
         '(1 cafeobj-start-face t t) '(2 cafeobj-file-face t t))
   (list (concat "^\\s-*\\<\\(quit\\|q\\|eof\\|popd\\|pwd\\)\\\\s-*$")
         '(1 cafeobj-start-face t t))
   (list (concat (cafeobj-flk-keyword "cd\\|push") cafeobj-flk-directory "$")
         '(1 cafeobj-start-face t t) '(2 cafeobj-file-face t t))
   (list (concat (cafeobj-flk-keyword "ls") "\\(.*?\\)\\s-+" cafeobj-flk-directory "$")
         '(1 cafeobj-start-face t t) '(2 font-lock-builtin-face t t) '(3 cafeobj-file-face t t))
;;; COMMANDS
   (list (concat (cafeobj-flk-keyword "select") cafeobj-flk-name cafeobj-flk-end-command)
         '(1 cafeobj-start-face t t) '(2 cafeobj-module-name-face t t) '(3 cafeobj-end-face t t))
   (list (concat (cafeobj-flk-keyword "load") cafeobj-flk-file-name "$")
         '(1 cafeobj-start-face t t) '(2 cafeobj-file-face t t))
   ;; 	 (list (concat (cafeobj-flk-keyword "in") cafeobj-flk-file-name)
   ;; 				 '(1 cafeobj-start-face t t) '(2 cafeobj-module-name-face t t))
   (list (concat (cafeobj-flk-keyword "parse") cafeobj-flk-in-module "\\(:\\)" cafeobj-flk-term cafeobj-flk-end-command)
         '(1 cafeobj-start-face t t) '(3 font-lock-keyword-face t t) '(4 cafeobj-module-name-face t t) '(6 cafeobj-end-face))
   (list (concat cafeobj-flk-debug (cafeobj-flk-keyword "red\\|reduce")
                 cafeobj-flk-in-module "\\(:\\)" cafeobj-flk-term cafeobj-flk-end-command)
         '(1 cafeobj-start-face t t) '(2 cafeobj-start-face t t) '(4 font-lock-keyword-face) '(5 cafeobj-module-name-face t t)
         '(6 font-lock-keyword-face t t) '(8 cafeobj-end-face))
   (list (concat cafeobj-flk-debug (cafeobj-flk-keyword "red\\|reduce")
                 cafeobj-flk-term cafeobj-flk-end-command)
         '(1 cafeobj-start-face t t) '(2 cafeobj-start-face t t)  '(4 cafeobj-end-face))
   (list (concat cafeobj-flk-debug (cafeobj-flk-keyword "rew\\|rewrite") cafeobj-flk-number-in-square cafeobj-flk-in-module
                 cafeobj-flk-term-possibly-two-lines cafeobj-flk-end-command)
         '(1 cafeobj-start-face prepend t) ; debug
         '(2 cafeobj-start-face prepend t) ; rew
         '(3 font-lock-builtin-face prepend t) ; [10]
         '(5 font-lock-keyword-face prepend t) ; in
         '(6 cafeobj-module-name-face prepend t)
         '(7 cafeobj-end-face prepend t))
   (list (concat cafeobj-flk-debug (cafeobj-flk-keyword "frew\\|frewrite")
                 "\\(\\[[0-9, ]+\\]\\)?\\s-+" ; Note the regexp [10, 10]
                 cafeobj-flk-in-module
                 cafeobj-flk-term-possibly-two-lines cafeobj-flk-end-command)
         '(1 cafeobj-start-face prepend t) ; debug
         '(2 cafeobj-start-face prepend t) ; rew
         '(3 font-lock-builtin-face prepend t) ; [10]
         '(5 font-lock-keyword-face prepend t) ; in
         '(6 cafeobj-module-name-face prepend t)
         '(7 cafeobj-end-face prepend t))
   (list (concat (cafeobj-flk-keyword "x?match") cafeobj-flk-number-in-square
                 cafeobj-flk-in-module cafeobj-flk-term "\\(<=\\?\\)" "\\(.+?\\)\\s-"
                 cafeobj-flk-such-that-condition cafeobj-flk-end-command)
         '(1 cafeobj-start-face t t) '(2 font-lock-builtin-face t t) '(4 font-lock-keyword-face t t) '(5 cafeobj-module-name-face t t)
         '(7 cafeobj-start-face t t)      ; <=?
         '(10 font-lock-keyword-face t t)                  ; such that
         '(12 cafeobj-end-face t t))                         ; such that
   (list (concat "(?" (cafeobj-flk-keyword "search") cafeobj-flk-number-in-square
                 cafeobj-flk-in-module cafeobj-flk-term "\\(=>[!+*1]\\)" "\\(.+?\\)\\s-"
                 cafeobj-flk-such-that-condition cafeobj-flk-end-command)
         '(1 cafeobj-start-face t t) '(2 font-lock-builtin-face t t) '(4 font-lock-keyword-face t t) '(5 cafeobj-module-name-face t t)
         '(7 cafeobj-start-face t t)                                ; =>
         '(10 font-lock-keyword-face t t) ; such that
         '(12 cafeobj-pattern-face t t)
         '(12 cafeobj-end-face t t))
   (list (concat (cafeobj-flk-keyword "continue\\|cont") "\\([1-9][0-9]\\)*\\s-+" cafeobj-flk-end-command)
         '(1 cafeobj-start-face t t) '(2 font-lock-builtin-face t t) '(3 cafeobj-end-face t t))
   (list (concat (cafeobj-flk-keyword "loop") cafeobj-flk-in-module cafeobj-flk-term cafeobj-flk-end-command)
         '(1 cafeobj-start-face t t) '(3 font-lock-keyword-face t t) '(4 cafeobj-module-name-face t t) '(6 cafeobj-end-face t t))
   (list (concat (cafeobj-flk-keyword "trace") "\\<\\(select\\|deselect\\|include\\|exclude\\)\\>\\s-+" cafeobj-flk-mod-exp cafeobj-flk-end-command)
         '(1 cafeobj-start-face t t) '(2 font-lock-keyword-face t t) '(4 cafeobj-end-face t t))
   (list (concat (cafeobj-flk-keyword "print") "\\<\\(conceal\\|reveal\\)\\>\\s-+" cafeobj-flk-mod-exp cafeobj-flk-end-command)
         '(1 cafeobj-start-face t t) '(2 font-lock-keyword-face t t) '(4 cafeobj-end-face t t))
   (list (concat (cafeobj-flk-keyword "break") "\\<\\(select\\|deselect\\)\\>\\s-+" cafeobj-flk-mod-exp cafeobj-flk-end-command)
         '(1 cafeobj-start-face t t) '(2 font-lock-keyword-face t t) '(4 cafeobj-end-face t t))
   (list (concat (cafeobj-flk-keyword "show") cafeobj-flk-term cafeobj-flk-end-command)
         '(1 cafeobj-start-face t t) '(2 'default t t) '(3 cafeobj-end-face t t))
   (list (concat (cafeobj-flk-keyword "show") (cafeobj-flk-keyword "modules") cafeobj-flk-end-command)
         '(1 cafeobj-start-face t t) '(2 font-lock-keyword-face t t) '(3 cafeobj-end-face t t))
   (list (concat (cafeobj-flk-keyword "show") (cafeobj-flk-keyword "search\\s-+graph") cafeobj-flk-end-command)
         '(1 cafeobj-start-face t t) '(2 font-lock-keyword-face t t) '(3 cafeobj-end-face t t))
   (list (concat (cafeobj-flk-keyword "show") (cafeobj-flk-keyword "path") "\\([1-9][0-9]*\\)\\s-+" cafeobj-flk-end-command)
         '(1 cafeobj-start-face t t) '(2 font-lock-keyword-face t t) '(3 font-lock-builtin-face t t) '(4 cafeobj-end-face t t))
   (list (concat (cafeobj-flk-keyword "do") (cafeobj-flk-keyword "clear\\s-+memo") cafeobj-flk-end-command)
         '(1 cafeobj-start-face t t) '(2 font-lock-keyword-face t t) '(3 cafeobj-end-face t t))
   (list (concat (cafeobj-flk-keyword "set") (cafeobj-flk-keyword "show\\|print\\|trace\\|include") cafeobj-flk-mod-exp cafeobj-flk-on-off cafeobj-flk-end-command)
         '(1 cafeobj-start-face t t) '(2 cafeobj-attribute-face t t) '(3 cafeobj-attribute-face t t) '(4 cafeobj-attribute-value-face t t) '(5 cafeobj-end-face t t))
   (list (concat (cafeobj-flk-keyword "set") (cafeobj-flk-keyword "break") cafeobj-flk-on-off cafeobj-flk-end-command)
         '(1 cafeobj-start-face t t) '(2 cafeobj-attribute-face t t) '(3 cafeobj-attribute-value-face t t) '(4 cafeobj-end-face t t))
;;; DEBUGGER COMMANDS
   (list (concat (cafeobj-flk-keyword "resume\\|abort\\|step\\|where") cafeobj-flk-end-command)
         '(1 cafeobj-start-face t t) '(2 cafeobj-end-face t t))
;;; MODULE
   (list "(?\\(mod\\*\\|mod!\\)\\s-+\\(.+\\)\\s-+\\({\\)"
         '(1 cafeobj-start-face prepend t)
         '(2 cafeobj-module-name-face prepend t)
         '(3 cafeobj-start-face prepend t))
   (list  "\\(}\\)"
          '(1 cafeobj-start-face prepend t))

;;; MODULE: IMPORT
   (list (concat "\\(extending\\|protecting\\|including\\|inc\\|pr\\|ex\\)\\(([^}]*)\\)")
         '(1 font-lock-keyword-face append t) 
	 '(2 font-lock-type-face append t) ; (
	 '(3 cafeobj-module-name-face prepend t)
	 '(4 font-lock-type-face append t)) ; )

;;; MODULE * TYPES
;;;   (list (concat (cafeobj-flk-keyword "sorts?") "\\(\\([a-zA-Z0-9(){},<>-]+\\s-+\\)+\\)" cafeobj-flk-end) ; The double \\(\\) because font-lock only match once a line
;;;         '(1 font-lock-keyword-face)
;;;         '(2 font-lock-type-face prepend t)
;;;         '(4 cafeobj-end-face prepend t))


 (list  "\\[\\(\\([a-zA-Z0-9(){},<>-]+\\|[a-zA-Z0-9(){},<>-]+\\s-+\\)+[a-zA-Z0-9(){},<>-]+\\)\\]" 
	'(1 font-lock-type-face prepend t))


;;; (list  "\\[\\(\\([a-zA-Z0-9(){},<>-]+\\s-+\\)+\\)\\]" 
;;;	'(1 font-lock-type-face prepend t))

   ;; subsort.  Havent found good way to do this without colorizing the >s.
   (list "\\(\\<subsorts?\\>\\)\\s-\\(.+\\)\\(\\.\\)" '(1 font-lock-keyword-face) '(2 cafeobj-element-face prepend t) '(3 cafeobj-end-face prepend t))
   (list "\\(\\<subsorts?\\>\\)[^<]+\\(<\\).+\\(\\.\\)" '(1 font-lock-keyword-face) '(2 'default prepend t) '(3 cafeobj-end-face prepend t))
   (list "\\(\\<subsorts?\\>\\)[^<]+<[^<]+\\(<\\).+\\(\\.\\)" '(1 font-lock-keyword-face) '(2 'default prepend t) '(3 cafeobj-end-face prepend t))
   (list "\\(\\<subsorts?\\>\\)[^<]+<[^<]+<[^<]+\\(<\\).+\\(\\.\\)" '(1 font-lock-keyword-face) '(2 'default prepend t) '(3 cafeobj-end-face prepend t))
   (list "\\(\\<subsorts?\\>\\)[^<]+<[^<]+<[^<]+<[^<]+\\(<\\).+\\(\\.\\)" '(1 font-lock-keyword-face) '(2 'default prepend t) '(3 cafeobj-end-face prepend t))
   (list "\\(\\<subsorts?\\>\\)[^<]+<[^<]+<[^<]+<[^<]+<[^<]+\\(<\\).+\\(\\.\\)" '(1 font-lock-keyword-face) '(2 'default prepend t) '(3 cafeobj-end-face prepend t))
   ;; Hmm.  Doesnt work
   ;; 	 (list (concat "\\(subsorts?\\)\\s-+" cafeobj-flk-type-name "\\(\\(<\\)\\s-+" cafeobj-flk-type-name "\\)+" cafeobj-flk-end)
   ;; 				 '(1 cafeobj-module-name-face t t)
   ;; 				 '(2 'default append append)
   ;; 				 '(3 cafeobj-element-face prepend t)
   ;; 				 '(4 definiendum-face append ())
   ;; 				 '(5 cafeobj-element-face prepend t)
   ;; 				 '(6 cafeobj-end-face prepend t))
   ;;  	 ;; subsorts.  Silly way to do this.  Anyone better?
   ;;  	 (list "\\(\\<subsorts?\\>\\)\\([^<]+\\)\\s-+\\(\\.\\)" '(1 font-lock-keyword-face) '(2 cafeobj-element-face prepend t) '(3 cafeobj-end-face prepend t))
   ;;  	 (list "\\(\\<subsorts?\\>\\)\\s-+[^<]+<\\([^<]+\\)\\s-+\\(\\.\\)" '(1 font-lock-keyword-face) '(2 cafeobj-element-face prepend t) '(3 cafeobj-end-face prepend t))
   ;;  	 (list "\\(\\<subsorts?\\>\\)\\s-+[^<]+<[^<]+<\\([^<]+\\)\\s-+\\(\\.\\)" '(1 font-lock-keyword-face) '(2 cafeobj-element-face prepend t) '(3 cafeobj-end-face prepend t))
   ;;  	 (list "\\(\\<subsorts?\\>\\)\\s-+[^<]+<[^<]+<[^<]+<\\([^<]+\\)\\s-+\\(\\.\\)" '(1 font-lock-keyword-face) '(2 cafeobj-element-face prepend t) '(3 cafeobj-end-face prepend t))
   ;;  	 (list "\\(\\<subsorts?\\>\\)\\s-+[^<]+<[^<]+<[^<]+<[^<]+<\\([^<]+\\)\\s-+\\(\\.\\)" '(1 font-lock-keyword-face) '(2 cafeobj-element-face prepend t) '(3 cafeobj-end-face prepend t))
   ;;  	 (list "\\(\\<subsorts?\\>\\)\\s-+[^<]+<[^<]+<[^<]+<[^<]+<[^<]+<\\([^<]+\\)\\s-+\\(\\.\\)" '(1 font-lock-keyword-face) '(2 cafeobj-element-face prepend t) '(3 cafeobj-end-face prepend t))
   ;;  	 (list "\\(\\<subsorts?\\>\\)\\s-+[^<]+<[^<]+<[^<]+<[^<]+<[^<]+<[^<]+<\\([^<]+\\)\\s-+\\(\\.\\)" '(1 font-lock-keyword-face) '(2 cafeobj-element-face prepend t) '(3 cafeobj-end-face prepend t))
   ;;  	 (list "\\(\\<subsorts?\\>\\)\\s-+[^<]+<[^<]+<[^<]+<[^<]+<[^<]+<[^<]+<[^<]+<\\([^<]+\\)\\s-+\\(\\.\\)" '(1 font-lock-keyword-face) '(2 cafeobj-element-face prepend t) '(3 cafeobj-end-face prepend t))
   ;;  	 (list "\\(\\<subsorts?\\>\\)\\s-+[^<]+<[^<]+<[^<]+<[^<]+<[^<]+<[^<]+<[^<]+<[^<]+<\\([^<]+\\)\\s-+\\(\\.\\)" '(1 font-lock-keyword-face) '(2 cafeobj-element-face prepend t) '(3 cafeobj-end-face prepend t))
   ;; classs    ; could be made more effective?
   (list "\\<\\(\\<class\\)\\s-+\\(.+\\)|"
         '(2 font-lock-type-face prepend t))
   ;; 	 ;; subclasss
   ;; 	 (list "\\<\\(subclass\\)\\>\\([^<]+\\)"
   ;; 				 '(2 font-lock-type-face prepend t))
   ;; 	 (list "\\<subclasses\\>\\([^<]+\\)"
   ;; 				 '(2 font-lock-type-face prepend t))
;;; MODULE * OPERATORS
   (list (concat (cafeobj-flk-keyword "b?ops?") "\\(.*\\)\\s-"
                 "\\(:\\)\\s-+\\(\\([0-9a-zA-Z@$'_,<>-]+\\s-+\\)*\\)" ;;; \\(" cafeobj-flk-type-name "\\)*"
                 "\\([-~]>\\)\\s-+" cafeobj-flk-type-name
                 "\\({[^}]*\\}\\s-+\\)?" cafeobj-flk-end)
         '(1 font-lock-keyword-face prepend t)
         '(2 font-lock-function-name-face prepend t)
         '(3 font-lock-keyword-face prepend t) ; :
         '(4 font-lock-type-face prepend t)
         '(6 font-lock-keyword-face prepend t) ; ->
         '(7 font-lock-type-face prepend t)
         '(8 font-lock-keyword-face prepend t)
         '(9 cafeobj-end-face prepend t))
   ;; Attr
   (list (cafeobj-flk-attribute "assoc\\|associative") '(1 cafeobj-attribute-face prepend t))
   (list (cafeobj-flk-attribute "comm\\|commutative") '(1 cafeobj-attribute-face prepend t))
   (list (cafeobj-flk-attribute-colon-value "id" "[^]\n]*")       '(1 cafeobj-attribute-face prepend t) '(2 cafeobj-attribute-face t t))
   (list (cafeobj-flk-attribute-colon-value "\\(\\<left\\>\\|\\<right\\>\\)\\s-+id" "{^}\n}*") ; Need to be before the other attributes in the elisp code
         '(1 cafeobj-attribute-face prepend t) '(2 cafeobj-attribute-face prepend t) '(3 cafeobj-attribute-face prepend t))
   (list (cafeobj-flk-attribute "idem\\|idempotent") '(1 cafeobj-attribute-face prepend t))
   (list (cafeobj-flk-attribute "iter\\|iterated") '(1 cafeobj-attribute-face prepend t))
   (list (cafeobj-flk-attribute "memo") '(1 cafeobj-attribute-face prepend t))
   (list (cafeobj-flk-attribute "ditto") '(1 cafeobj-attribute-face prepend t))
   (list (cafeobj-flk-attribute-value "poly" "([ 0-9]+)")       '(1 cafeobj-attribute-face prepend t)      '(2 cafeobj-attribute-value-face prepend t))
   (list (cafeobj-flk-attribute-value "strat\\|strategy" "([ 0-9]+)")       '(1 cafeobj-attribute-face prepend t)      '(2 cafeobj-attribute-value-face prepend t))
   (list (cafeobj-flk-attribute-value "gather" "([ eE&]+)")       '(1 cafeobj-attribute-face prepend t)      '(2 cafeobj-attribute-value-face prepend t))
   (list "\\[.*\\(\\<format\\)\\s-+\\(([^)]+)\\).*\\]"     '(1 cafeobj-attribute-face prepend t)        '(2 cafeobj-attribute-value-face prepend t))
   (list (cafeobj-flk-attribute-value "special" "(.+)")       '(1 cafeobj-attribute-face prepend t)      '(2 cafeobj-attribute-value-face prepend t))
   ;; StatementAttr elsewhere: nonexec, otherwise, metadata, label
   (list (cafeobj-flk-attribute-value "prec\\|precedence" "[0-9]+")   '(1 cafeobj-attribute-face prepend t)	 '(2 cafeobj-attribute-value-face prepend t))
   ;; 	 (list "\\[.*\\(\\<id:\\)\\s-+\\(\\w+\\).*\\]"   '(1 cafeobj-attribute-face prepend t)   '(2 cafeobj-attribute-value-face prepend t))
   (list (cafeobj-flk-attribute "ctor\\|constructor") '(1 cafeobj-element-face prepend t))
   (list (cafeobj-flk-attribute "frozen") '(1 cafeobj-attribute-face prepend t))
   (list (cafeobj-flk-attribute-value "frozen" "([ 0-9]+)")       '(1 cafeobj-attribute-face prepend t)      '(2 cafeobj-attribute-value-face prepend t))
;;; MODULE * VARIABLES
   (list (concat (cafeobj-flk-keyword "vars?")
                 "\\(\\([0-9a-zA-Z@$'_,<>-]+\\s-+\\)*\\)"
                 "\\(:\\)\\s-+" cafeobj-flk-type-name cafeobj-flk-end)
         '(1 font-lock-keyword-face prepend t)
         '(2 font-lock-variable-name-face prepend t)
         '(4 font-lock-keyword-face prepend t)
         '(5 font-lock-type-face prepend t)
         '(6 cafeobj-end-face prepend t))
;;; MODULE * MEMBERSHIP
   (list (concat "\\<\\(mb\\)\\>\\s-+?" cafeobj-flk-label cafeobj-flk-pattern
                 "\\(:\\)\\s-+?" cafeobj-flk-type-name cafeobj-flk-end)
         '(1 font-lock-keyword-face prepend t) ; mb
         '(3 cafeobj-label-face prepend t)       ; [label]
         '(4 font-lock-keyword-face prepend t) ; :
         '(5 cafeobj-pattern-face prepend t)
         '(6 font-lock-keyword-face prepend t) ; :
         '(7 cafeobj-element-face prepend t)
         '(8 cafeobj-end-face prepend t))
   (list (concat "\\<\\(cmb\\)\\>\\s-+?" cafeobj-flk-label cafeobj-flk-pattern
                 "\\(:\\)\\s-+?" cafeobj-flk-type-name
                 "\\(if\\)\\s-+" cafeobj-flk-pattern cafeobj-flk-end)
         '(1 font-lock-keyword-face prepend t) ;cmb
         '(3 cafeobj-label-face prepend t)       ; [label]
         '(4 font-lock-keyword-face prepend t) ; :
         '(5 cafeobj-pattern-face prepend t)
         '(6 font-lock-keyword-face prepend t) ; :
         '(7 cafeobj-element-face prepend t)
         '(8 font-lock-keyword-face prepend t) ; if
         '(9 cafeobj-pattern-face prepend t)
         '(10 cafeobj-end-face prepend t))
;;; MODULE * EQUATIONS
   (list (concat "\\(\\<eq\\>\\)\\s-+?" cafeobj-flk-label cafeobj-flk-pattern
                 "\\(=\\)")
         '(1 font-lock-keyword-face prepend t) ; eq
         '(3 cafeobj-label-face prepend t)       ; [label]
         '(4 font-lock-keyword-face prepend t) ; :
         '(5 cafeobj-pattern-face prepend t)     ; pattern
         '(6 font-lock-keyword-face prepend t) ; =
         )
   (list (concat "\\<\\(ceq\\|cq\\)\\>\\s-+?" cafeobj-flk-label cafeobj-flk-pattern
                 "\\(=\\)\\s-+")
         '(1 font-lock-keyword-face prepend t)
         '(3 cafeobj-label-face prepend t) ; [label]
         '(4 font-lock-keyword-face prepend t) ; :
         '(5 cafeobj-pattern-face prepend t)
         '(6 font-lock-keyword-face prepend t) ; =
         )
                                        ; Statement Attr (as opposed to attr)
   (list (cafeobj-flk-attribute "nonexec")
         '(1 cafeobj-attribute-face prepend t))
   (list (cafeobj-flk-attribute "owise\\|otherwise")
         '(1 cafeobj-attribute-face prepend t))
   (list (cafeobj-flk-attribute-value "metadata" "\\w+")
         '(1 cafeobj-attribute-face prepend t)
         '(2 cafeobj-attribute-value-face prepend t))
   (list (cafeobj-flk-attribute-value "label" "\\w+")
         '(1 cafeobj-attribute-face prepend t)
         '(2 cafeobj-attribute-value-face prepend t))
;;; MODULE * RULES
                                        ; rl [rule-name] : pattern => result .
   (list (concat "\\(\\<c?rl\\>\\)\\s-+?" cafeobj-flk-label)
         '(1 font-lock-keyword-face) ; rl
         '(3 cafeobj-label-face)       ; [label]
         '(4 font-lock-keyword-face) ; :
         )
    (list "\\s-\\(=>\\)\\s-"
          '(1 font-lock-keyword-face prepend t)) ; =>
;;; END OF CORE cafeobj GRAMMAR
;;; FULL cafeobj
   ;; Don't have the full cafeobj grammar here, but try to include something
   (list "(\\(omod\\|fth\\|th\\|oth\\) \\(.+\\) \\(is\\)"
         '(1 cafeobj-start-face prepend t)
         '(2 cafeobj-module-name-face prepend t)
         '(3 cafeobj-start-face prepend t))
   (list  "\\(endom\\|endfth\\|endth\\|endoth\\))"
          '(1 cafeobj-start-face prepend t))
   (list (concat (cafeobj-flk-keyword "sort\\|class") cafeobj-flk-type-name
                 (cafeobj-flk-keyword "to") cafeobj-flk-type-name cafeobj-flk-end)
         '(1 font-lock-keyword-face prepend t) ; sort
         '(2 font-lock-type-face prepend t)    ;
         '(3 font-lock-keyword-face prepend t) ; to
         '(4 font-lock-type-face prepend t)
         '(5 cafeobj-end-face prepend t)) ; .
   (list (concat (cafeobj-flk-keyword "op\\|msg") cafeobj-flk-term
                 (cafeobj-flk-keyword "to") cafeobj-flk-term cafeobj-flk-end)
         '(1 font-lock-keyword-face prepend t) ; op
         '(2 font-lock-function-name-face prepend t) ;
         '(3 font-lock-keyword-face prepend t)       ; to
         '(4 font-lock-function-name-face prepend t)
         '(5 cafeobj-end-face prepend t)) ; .
   (list (concat (cafeobj-flk-keyword "attr") cafeobj-flk-name "\\(\\.\\)\\s-+" cafeobj-flk-type-name
                 (cafeobj-flk-keyword "to") cafeobj-flk-name cafeobj-flk-end)
         '(1 font-lock-keyword-face prepend t)                 ; attr
         '(2 cafeobj-attribute-face prepend t)                         ;
         '(3 font-lock-keyword-face prepend t)                 ; .
         '(4 font-lock-type-face prepend t)
         '(5 font-lock-keyword-face prepend t) ; to
         '(6 cafeobj-attribute-face prepend t)
         '(7 cafeobj-end-face prepend t)) ; .
   ;; OTHER STUFF
   ;; if then else
   (list (concat (cafeobj-flk-keyword "if") cafeobj-flk-pattern
                 (cafeobj-flk-keyword "then") cafeobj-flk-pattern
                 "\\(" (cafeobj-flk-keyword "else") cafeobj-flk-pattern
                 "\\)?" (cafeobj-flk-keyword "fi"))
         '(1 font-lock-keyword-face prepend t)
         '(2 cafeobj-pattern-face prepend t)
         '(3 font-lock-keyword-face prepend t) ;then
         '(6 font-lock-keyword-face prepend t) ; else
         '(8 font-lock-keyword-face prepend t)) ; fi
   ;; if condition for ceq, crl
   (list (concat (cafeobj-flk-keyword "if") cafeobj-flk-pattern cafeobj-flk-end)
         '(1 font-lock-keyword-face)
         '(2 cafeobj-pattern-face)
         '(3 cafeobj-end-face))
;;; WARNINGS
   ;;    Remove this if it causes too much confusion.  Ellef 2004-06-20
   (list cafeobj-warnings '(1 font-lock-warning-face prepend t))
   (list "\\(\\.\\)\\s-*$" '(1 'bold append t)) ;; To be removed
   (list "\\(\\.\\)\\s-*\\*\\*\\*" '(1 'bold append t)) ;; To be removed
   ;; COMMENTS
   (list "\\([--]\\{2\\}.*$\\)" '(1 font-lock-comment-face t t))
   (list "\\((\\-\\{2\\}.*\\*)\\)" '(1 font-lock-comment-face t t))
   (list "\\((\\-\\{2\\}.*\\)" '(1 font-lock-comment-face t t)) ; Poor-man multiline comments: Just the first and last line
   (list "\\(.*\\*)\\)" '(1 font-lock-comment-face t t)) ; End of multiline comment
   (list "\\(\\*\\{4\\}.*\\)" '(1 cafeobj-comment-highlight-face t t)) ; Highlight ****
   (list "\\(\\*\\{5\\}.*\\)" '(1 cafeobj-comment-highlight-highlight-face t t)) ; Highlight *****
   (list "\\(\\*\\*\\*\\s-*(\\)" '(1 font-lock-warning-face t t)) ; Dirty bug in cafeobj 2.1.
                                        ; 	 (cons (concat "\\<\\("
                                        ; 								 (eval-when-compile
                                        ; 									 (regexp-opt '(":" "<" "." "->" "=")))
                                        ; 								 "\\)\\>") 'font-lock-keyword-face)
   ;; 	 (list "\\(\\*\\*\\*.*)\\)"                         ; The famous *** foo ) error in cafeobj
   ;; 				 '(1 font-lock-warning-face t t))
   )
  "Subdued level highlighting for cafeobj mode.")

;;; -------------
;;; MENU SUPPORTS_______________________________________________________________
;;; -------------

;; For XEmacs 
(defvar cafeobj-mode-popup-menu nil)
(defvar cafeobj-mode-menubar-menu nil)

;; For FSF19
(defvar cafeobj-mode-menu nil
  "Keymap for cafeobj-mode's menu.")

;; (cond (cafeobj-xemacs-p
;;        (setq cafeobj-mode-popup-menu
;; 	     (purecopy '("CafeOBJ Interaction Menu"
;; 			 ["Evaluate This Declaration"
;; 			  cafeobj-send-decl          t]
;; 			 ["Evaluate Current Line" cafeobj-send-line  t]
;; 			 ["Evaluate Entire Buffer" cafeobj-send-buffer t]
;; 			 ["Evaluate Region"  cafeobj-send-region
;; 			  (region-exists-p)]
;; 			 "---"			 
;; 			 ["Comment Out Region"	comment-region	(region-exists-p)]
;; 			 ["Indent Region"	indent-region	(region-exists-p)]
;; 			 ["Indent Line"		cafeobj-indent-line t]
;; 			 ["Beginning of Declaration"
;; 			  cafeobj-beginning-of-decl t]
;; 			 )))
;;        (setq cafeobj-mode-menubar-menu
;; 	     (purecopy (cons "CafeOBJ" (cdr cafeobj-mode-popup-menu)))))
;;       (t ;; 
;;        (setq cafeobj-mode-menu (make-sparse-keymap "CafeOBJ"))
;;        (define-key cafeobj-mode-menu [choas-send-line]
;; 	 '("Evaluate Current Line" . cafeobj-send-current-line))
;;        (define-key cafeobj-mode-menu [cafeobj-send-region]
;; 	 '("Evaluate Cafeobj-Region" . cafeobj-send-region))
;;        (define-key cafeobj-mode-menu [cafeobj-send-proc]
;; 	 '("Evaluate This Declaration" . cafeobj-send-decl))
;;        (define-key cafeobj-mode-menu [cafeobj-send-buffer]
;; 	 '("Send Buffer" . cafeobj-send-buffer))
;;        (define-key cafeobj-mode-menu [cafeobj-beginning-of-decl]
;; 	 '("Beginning Of Proc" . cafeobj-beginning-of-decl))
;;        (define-key cafeobj-mode-menu [comment-region]
;; 	 '("Comment Out Region" . comment-region))
;;        (define-key cafeobj-mode-menu [indent-region]
;; 	 '("Indent Region" . indent-region))
;;        (define-key cafeobj-mode-menu [cafeobj-indent-line]
;; 	 '("Indent Line" . cafeobj-indent-line))
;;        (define-key cafeobj-mode-menu [cafeobj-beginning-of-decl]
;; 	 '("Beginning of Declaration" . cafeobj-beginning-of-decl))
;;        ))

;;; ------------
;;; CafeOBJ Mode________________________________________________________________
;;; ------------

(defvar cafeobj-mode-abbrev-table nil
  "Abbrev table in use in CafeOBJ mode.")
;;; some default abbreviations define here
(if cafeobj-mode-abbrev-table
    nil
    (define-abbrev-table 'cafeobj-mode-abbrev-table
	'(("btrns" "btrans" nil 0)
	  ("bcq" "bceq" nil 0)
	  ("compat" "compatibility" nil 0)
	  ("psort" "principal-sort" nil 0)
	  ("req" "require" nil 0)
	  ("sh" "show" nil 0)
	  ("verb" "verbose" nil 0)
	  ("reg" "regularize" nil 0)
	  ("import" "imports" nil 0)
	  ("cq" "ceq" nil 0)
	  ("red" "reduce" nil 0)
	  ("strat" "strategy" nil 0)
	  ("us" "using" nil 0)
	  ("btrn" "btrans" nil 0)
	  ("btr" "btrans" nil 0)
	  ("reconst" "reconstruct" nil 0)
	  ("chk" "check" nil 0)
	  ("trns" "trans" nil 0)
	  ("swit" "swithces" nil 0)
	  ("bq" "beq" nil 0)
	  ("desc" "describe" nil 0)
	  ("bctr" "bctrans" nil 0)
	  ("incl" "include" nil 0)
	  ("bctrn" "bctrans" nil 0)
	  ("ex" "extending" nil 0)
	  ("axs" "axioms" nil 0)
	  ("pr" "protecting" nil 0)
	  ("bctrns" "bctrans" nil 0)
	  ("pat" "pattern" nil 0)
	  ("recon" "reconstruct" nil 0)
	  ("prov" "provide" nil 0)
	  ("sel" "select" nil 0)
	  ("sig" "signature" nil 0)
	  ("sign" "signature" nil 1)
	  ("cond" "conditions" nil 0)
	  ("imp" "imports" nil 0)
	  ("comp" "compatibility" nil 0)
	  ("swi" "switch" nil 0)
	  ("reconstr" "reconstruct" nil 0)
	  ))
    )


(defvar cafeobj-mode-syntax-table nil
  "Syntax table in use in cafeobj-mode buffers.")

(if cafeobj-mode-syntax-table
    ()
  (setq cafeobj-mode-syntax-table (make-syntax-table))
  (mapcar (function
	   (lambda (x) (modify-syntax-entry
			(car x) (cdr x) cafeobj-mode-syntax-table)))
	  '(( ?\( . "()" ) ( ?\) . ")(" )
	    ( ?\[ . "(]" ) ( ?\] . ")[" )
	    ( ?\{ . "(}" ) ( ?\} . "){" )
	    ;; underscore is word class
	    ( ?\_ . "w" )
	    ( ?\" . "\"" )	; double quote is string quote too
	    ( ?\n . ">"))
	  ))


(define-derived-mode cafeobj-mode fundamental-mode "CafeOBJ"
  :group 'cafeobj
  :syntax-table cafeobj-mode-syntax-table
  (setq font-lock-defaults '(cafeobj-font-lock-keywords))

  ;; indentation
  (define-key cafeobj-mode-map [?}] 'cafeobj-electric-brace)
  ;; communication
  (define-key cafeobj-mode-map (kbd "C-c i") 'cafeobj-send-line)
  (define-key cafeobj-mode-map (kbd "C-c e") 'cafeobj-send-decl)
  (define-key cafeobj-mode-map (kbd "C-c r") 'cafeobj-send-region)
  (define-key cafeobj-mode-map (kbd "C-c l") 'cafeobj-send-buffer)
  (define-key cafeobj-mode-map (kbd "C-c [") 'cafeobj-beginning-of-decl)
  (define-key cafeobj-mode-map (kbd "C-c ]") 'cafeobj-end-of-decl)
  (define-key cafeobj-mode-map (kbd "C-c q") 'cafeobj-kill-process)
  (define-key cafeobj-mode-map (kbd "C-c s") 'cafeobj-show-process-buffer)
  (define-key cafeobj-mode-map (kbd "C-c h") 'cafeobj-hide-process-buffer)

  ;; (define-key cafeobj-mode-map (kbd "C-c C-g") 'cafeobj-next-action)
  ;; (define-key cafeobj-mode-map (kbd "C-c C-r") 'cafeobj-send-region)
  ;; (define-key cafeobj-mode-map (kbd "C-M-x") 'cafeobj-send-definition)
  ;; (define-key cafeobj-mode-map (kbd "C-c C-b") 'cafeobj-send-buffer)
  ;; (define-key cafeobj-mode-map (kbd "C-c C-z") 'maude-switch-to-inferior-maude) 

  ;; Set up comments -- make M-; work
  (set (make-local-variable 'comment-start) "--")
  (set (make-local-variable 'comment-start-skip)
       "---+[ \t]*\\|\\*\\*\\*+[ \t]*")
  (set (make-local-variable 'comment-end) "")
  ;; Indentation
  (set (make-local-variable 'indent-line-function) 'cafeobj-indent-line)
  ;; Movement
  (set (make-local-variable 'beginning-of-defun-function)
       'cafeobj-beginning-of-defun)
  (set (make-local-variable 'end-of-defun-function) 'cafeobj-end-of-defun)
  ;; Abbrevs
  (setq local-abbrev-table cafeobj-mode-abbrev-table)
  ;; Menu
  (easy-menu-add cafeobj-mode-menu cafeobj-mode-map)
  ;; speedbar support
  (when (fboundp 'speedbar-add-supported-extension)
    (speedbar-add-supported-extension ".mod")))


;;; --------------------
;;; Default Key Bindings________________________________________________________
;;; --------------------



;; (defun cafeobj-setup-keymap ()
;;   "Set up keymap for cafeobj mode.
;; If the variable `cafeobj-prefix-key' is nil, the bindings go directly
;; to `cafeobj-mode-map', otherwise they are prefixed with `cafeobj-prefix-key'."
;;   (setq cafeobj-mode-map (make-sparse-keymap))
;;   ;; 
;;   (let ((map (if cafeobj-prefix-key
;; 		 (make-sparse-keymap)
;; 		 cafeobj-mode-map)))
;;     ;; indentation
;;     (define-key cafeobj-mode-map [?}] 'cafeobj-electric-brace)
;;     ;; communication
;;     (define-key map "i" 'cafeobj-send-line)
;;     (define-key map "e" 'cafeobj-send-decl)
;;     (define-key map "r" 'cafeobj-send-region)
;;     (define-key map "l" 'cafeobj-send-buffer)
;;     (define-key map "[" 'cafeobj-beginning-of-decl)
;;     (define-key map "]" 'cafeobj-end-of-decl)
;;     (define-key map "q" 'cafeobj-kill-process)
;;     (define-key map "s" 'cafeobj-show-process-buffer)
;;     (define-key map "h" 'cafeobj-hide-process-buffer)
;;     ;;
;;     (if cafeobj-prefix-key
;; 	(define-key cafeobj-mode-map cafeobj-prefix-key map))
;;     ))

;;; ----------------
;;; CafeOBJ Editting____________________________________________________________
;;; ----------------

(defconst cafeobj-auto-newline nil
  "*Non-nil means automatically newline before and after braces,
and after colons and semicolons, inserted in CafeOBJ code.")

(defconst cafeobj-tab-always-indent t
  "*Non-nil means TAB in C mode should always reindent the current line,
regardless of where in the line point is when the TAB command is used.")

(defun cafeobj-electric-brace (arg)
  "Insert character and correct line's indentation."
  (interactive "P")
  (let (insertpos)
    (if (and (not arg)
	     (eolp)
	     (or (save-excursion
		   (skip-chars-backward " \t")
		   (bolp))
		 (if cafeobj-auto-newline
		     (progn (cafeobj-indent-line) (newline) t)
		     nil)))
	(progn
	  (insert last-command-char)
	  (cafeobj-indent-line)
	  (if cafeobj-auto-newline
	      (progn
		(newline)
		;; (newline) may have done auto-fill
		(setq insertpos (- (point) 2))
		(cafeobj-indent-line)))
	  (save-excursion
	    (if insertpos (goto-char (1+ insertpos)))
	    (delete-char -1))))
    (if insertpos
	(save-excursion
	  (goto-char insertpos)
	  (self-insert-command (prefix-numeric-value arg)))
	(self-insert-command (prefix-numeric-value arg)))))

(defun cafeobj-beginning-of-block (&optional arg)
  "Move backward to the beginning of a CafeOBJ block structure.
With argument, do it that many times.  Negative arg -N
means move forward to Nth following beginning of declaration.
Returns t unless search stops due to beginning or end of buffer."
  (interactive "P")
  (or arg (setq arg 1))
  (let ((found nil)
	(ret t))
    (if (and (< arg 0)
	     (looking-at-cafeobj-block-start-pat))
	(forward-char 1))
    (while (< arg 0)
      (if (re-search-forward cafeobj-block-start-pat nil t)
	  (setq arg (1+ arg)
		found t)
	(setq ret nil
	      arg 0)))
    (if found
	(beginning-of-line))
    (while (> arg 0)
      (if (re-search-backward cafeobj-block-start-pat nil t)
	  (setq arg (1- arg))
	  (setq ret nil
		arg 0)))
    ret))

(defun cafeobj-beginning-of-decl (&optional arg)
  "Move backward to the beginning of a CafeOBJ top-level decl.
With argument, do it that many times.  Negative arg -N
means move forward to Nth following beginning of declaration.
Returns t unless search stops due to beginning or end of buffer."
  (interactive "P")
         (or arg (setq arg 1))
  (let ((found nil)
	(ret t))
    (if (and (< arg 0)
	     (looking-at cafeobj-top-decl-pat))
	(forward-char 1))
    (while (< arg 0)
      (if (re-search-forward cafeobj-top-decl-pat nil t)
	  (setq arg (1+ arg)
		found t)
	(setq ret nil
	      arg 0)))
    (if found
	(beginning-of-line))
    (while (> arg 0)
      (if (re-search-backward cafeobj-top-decl-pat nil t)
	  (setq arg (1- arg))
	  (setq ret nil
		arg 0)))
    ret))

(defun cafeobj-end-of-decl (&optional arg)
  "Move forward to next end of chaos declaration (or similar).
With argument, do it that many times.  Negative argument -N means move
back to Nth preceding end of proc.

This function just searches for a `}' at the beginning of a line."
  (interactive "P")
  (or arg
      (setq arg 1))
  (let ((found nil)
	(ret t))
    (if (and (< arg 0)
	     (not (bolp))
	     (save-excursion
	       (beginning-of-line)
	       (eq (following-char) ?})))
	(forward-char -1))
    (while (> arg 0)
      (if (re-search-forward "^}" nil t)
	  (setq arg (1- arg)
		found t)
	(setq ret nil
	      arg 0)))
    (while (< arg 0)
      (if (re-search-backward "^}" nil t)
	  (setq arg (1+ arg)
		found t)
	(setq ret nil
	      arg 0)))
    (if found
	(end-of-line))
    ret))

(defun cafeobj-outline-level ()
  (save-excursion
    (skip-chars-forward "\t ")
    (current-column)))

(defun cafeobj-inside-parens-p ()
  (condition-case ()
      (save-excursion
	(save-restriction
	  (narrow-to-region (point)
			    (progn (cafeobj-beginning-of-decl) (point)))
	  (goto-char (point-max))
	  (= (char-after (or (scan-lists (point) -1 1) (point-min))) ?\()))
    (error nil)))

(defun cafeobj-indent-command (&optional whole-exp)
  "Indent current line as CafeOBJ code, or in some cases insert a tab character.
If `cafeobj-tab-always-indent' is non-nil (the default), always indent current line.
Otherwise, indent the current line only if point is at the left margin or
in the line's indentation; otherwise insert a tab.

A numeric argument, regardless of its value, means indent rigidly all the
lines of the expression starting after point so that this line becomes
properly indented.  The relative indentation among the lines of the
expression are preserved."
  (interactive "P")
  (if whole-exp
      ;; If arg, always indent this line as CafeOBJ
      ;; and shift remaining lines of expression the same amount.
      (let ((shift-amt (cafeobj-indent-line))
	    beg end)
	(save-excursion
	  (if cafeobj-tab-always-indent
	      (beginning-of-line))
	  ;; Find beginning of following line.
	  (save-excursion
	    (forward-line 1) (setq beg (point)))
	  ;; Find first beginning-of-sexp for sexp extending past this line.
	  (while (< (point) beg)
	    (forward-sexp 1)
	    (setq end (point))
	    (skip-chars-forward " \t\n")))
	(if (> end beg)
	    (indent-code-rigidly beg end shift-amt "#")))
    (if (and (not cafeobj-tab-always-indent)
	     (save-excursion
	       (skip-chars-backward " \t")
	       (not (bolp))))
	(insert-tab)
      (cafeobj-indent-line))))

(defun cafeobj-indent-line ()
  "Indent current line as CafeOBJ code.
Return the amount the indentation changed by."
  (let ((indent (calculate-cafeobj-indent nil))
	beg shift-amt
	(case-fold-search nil)
	(pos (- (point-max) (point))))
    (beginning-of-line)
    (setq beg (point))
    (cond ((eq indent nil)
	   (setq indent (current-indentation)))
	  ((eq indent t)
	   (setq indent (current-indentation)))
	  (t
	   (skip-chars-forward " \t")
	   (if (listp indent) (setq indent (car indent)))
	   (cond ((= (following-char) ?})
		  (setq indent (- indent cafeobj-indent-level)))
		 ((= (following-char) ?{)
		  (setq indent (+ indent cafeobj-brace-offset))))))
    (skip-chars-forward " \t")
    (setq shift-amt (- indent (current-column)))
    (if (zerop shift-amt)
	(if (> (- (point-max) pos) (point))
	    (goto-char (- (point-max) pos)))
      (delete-region beg (point))
      (indent-to indent)
      ;; If initial point was within line's indentation,
      ;; position after the indentation.  Else stay at same point in text.
      (if (> (- (point-max) pos) (point))
	  (goto-char (- (point-max) pos))))
    shift-amt))

(defun calculate-cafeobj-indent (&optional parse-start)
  "Return appropriate indentation for current line as CafeOBJ code.
In usual case returns an integer: the column to indent to.
Returns nil if line starts inside a string, t if in a comment."
  (save-excursion
    (beginning-of-line)
    (let ((indent-point (point))
	  (case-fold-search nil)
	  state
	  containing-sexp)
      (if parse-start
	  (goto-char parse-start)
	(cafeobj-beginning-of-decl))
      (while (< (point) indent-point)
	(setq parse-start (point))
	(setq state (parse-partial-sexp (point) indent-point 0))
	(setq containing-sexp (car (cdr state))))
      ;;
      (cond ((or (nth 3 state) (nth 4 state))
	     ;; return nil or t if should not change this line
	     (nth 4 state))		; t if inside a comment, else nil.
	    ;; 
	    ((null containing-sexp)	; we are at top-level
	     ;; -- TOP-LEVEL------------------------------------------------
	     ;; Line is at top level.  May be module/view declaration or
	     ;; top-level commands.
	     (goto-char indent-point)	; start from original pos.
	     (skip-chars-forward " \t")
	     (cond ((= (following-char) ?{) 0)
		   ((looking-at cafeobj-top-decl-pat) 0)
		   ((looking-at cafeobj-comment-pat) (current-column))
		   (t
		    (cafeobj-backward-to-noncomment (or parse-start (point-min)))
		    ;; Look at previous line that's at column 0
		    ;; to determine whether we are in top-level decl.
		    (let ((basic-indent
			   (save-excursion
			     (re-search-backward "^[^ \^L\t\n]" nil 'move)
			     (if (and (looking-at cafeobj-top-decl-pat)
				      (not (progn
					     (condition-case nil
						 (progn
						   (search-forward "\{" parse-start)
						   (forward-list))
					       (error nil))
					     (looking-at "\}"))))
				 cafeobj-psort-indent
				 0))))
		      basic-indent))))
	    ;; NON TOPLEVEL ----------------------------------------------
	    ((and (/= (char-after containing-sexp) ?{)
		  (< (car state) 2))
	     ;; indent to just after the surrounding open.
	     (goto-char (+ 2 containing-sexp))
	     (current-column))
	    ;; WITHIN A BRACE --------------------------------------------
	    (t
	     (goto-char indent-point)
	     (if (and (not (is-cafeobj-beginning-of-statement))
		      (progn (cafeobj-backward-to-noncomment containing-sexp)
			     (not (memq (preceding-char)
					'(0 ?\, ?\} ?\{ ?\. ?\] ?\)))))
		      ;; don't treat a line with a close-brace
		      ;; as a continuation. It is probably the
		      ;; end of a block.
		      (save-excursion
			(goto-char indent-point)
			(skip-chars-forward " \t")
			(not (= (following-char) ?})))
		      )
		 ;; This line is continuation of preceding line's statement;
		 ;; indent  cafeobj-continued-statement-offset  more than the
		 ;; previous line of the statement.
		 (progn
		   (cafeobj-backward-to-start-of-continued-exp containing-sexp)
		   (+ cafeobj-continued-statement-offset (current-column)
		      (if (save-excursion (goto-char indent-point)
					  (skip-chars-forward " \t")
					  (eq (following-char) ?{))
			  cafeobj-continued-brace-offset
			  0)))
		 ;; This line starts a new statement.
		 ;; if we are looking at a comment line, leave it as is. 
		 (if (progn
		       (goto-char indent-point)
		       (skip-chars-forward " \t")
		       (looking-at cafeobj-comment-pat))
		     (current-column)
		     (progn
		       ;; Position following last unclosed open-brace.
		       (goto-char containing-sexp)
		       ;; Is line first statement after an open-brace?
		       (or
			;; If no, find that first statement and indent like it.
			(save-excursion
			  (forward-char 1)
			  (while (progn (skip-chars-forward " \t\n")
					(looking-at "--[ \t].*\\|\*\*[ \t].*"))
			    (forward-line 1))
			  ;; The first following code counts
			  ;; if it is before the line we want to indent.
			  (skip-chars-forward " \t\n")
			  (and (< (point) indent-point)
			       (- (current-column)
				  ;; If prev stmt starts with open-brace, that
				  ;; open brace was offset by cafeobj-brace-offset.
				  ;; Compensate to get the column where
				  ;; an ordinary statement would start.
				  (if (= (following-char) ?\{)
				      cafeobj-brace-offset
				      0))
			       ))
			;; If no previous statement,
			;; indent it relative to line brace is on.
			;; For open brace in column zero, don't let statement
			;; start there too.  If cafeobj-indent-level is zero,
			;; use cafeobj-brace-offset +
			;; cafeobj-continued-statement-offset instead.
			;; For open-braces not the first thing in a line,
			;; add in cafeobj-brace-imaginary-offset.
			(+ (if (and (bolp) (zerop cafeobj-indent-level))
			       (+ cafeobj-brace-offset
				  cafeobj-continued-statement-offset)
			       cafeobj-indent-level)
			   ;; Move back over whitespace before the openbrace.
			   ;; If openbrace is not first nonwhite thing on the line,
			   ;; add the cafeobj-brace-imaginary-offset.
			   (progn (skip-chars-backward " \t")
				  (if (bolp)
				      0
				      cafeobj-brace-imaginary-offset))
			   ;; If the openbrace is preceded by a parenthesized exp,
			   ;; move to the beginning of that;
			   ;; possibly a different line
			   (progn
			     (if (memq (preceding-char) '(?\) \]))
				 (forward-sexp -1))
			     ;; Get initial indentation of the line we are on.
			     (current-indentation))))))))))))

(defun is-cafeobj-beginning-of-statement ()
  (save-excursion
    (beginning-of-line)
    (skip-chars-forward " \t")
    (or (looking-at cafeobj-keyword-pat)
	(looking-at cafeobj-command-pat)
	(looking-at cafeobj-comment-pat))))

(defun cafeobj-backward-to-noncomment (lim)
  (let (stop)
    (while (not stop)
      (skip-chars-backward " \t\n\f" lim)
      (setq stop (or (<= (point) lim)
		     (save-excursion
		       (beginning-of-line)
		       (skip-chars-forward " \t")
		       (not (looking-at cafeobj-comment-pat)))))
      (or stop (beginning-of-line)))))

(defun cafeobj-backward-to-start-of-continued-exp (lim)
  (if (memq (preceding-char) '(?\) ?\" ?\]))
      (forward-sexp -1))
  (beginning-of-line)
  (if (<= (point) lim)
      (goto-char (1+ lim)))
  (skip-chars-forward " \t"))


;;; -----------------------
;;; RUN CafeOBJ Interpreter_____________________________________________________
;;; and Interact with it
;;; -----------------------
(defvar cafeobj-prompt-pattern "^[A-Za-z0-9!#$%']+> ")

(defun cafeobj-process-mode ()
  (interactive)
  (comint-mode)
  (setq major-mode 'CafeOBJ-process-mode)
  (setq mode-name "CafeOBJ process")
  (make-local-variable 'comint-prompt-regexp)
  ;;(cond (cafeobj-xemacs-p
  ;;	   (make-local-variable 'font-lock-defaults)
  ;;	   (put 'cafeobj-process-mode 'font-lock-defaults
  ;;	        '(cafeobj-font-lock-keywords)))
  ;;
  ;;   	 (t (set (make-local-variable 'font-lock-keywords)
  ;;		 cafeobj-font-lock-keywords)))
  (setq comint-prompt-regexp cafeobj-prompt-pattern)
  (setq local-abbrev-table cafeobj-mode-abbrev-table)
  (comint-read-input-ring t)
  )

(defun cafeobj-start-process (name program &optional startfile &rest switches)
  "Start a chaos process named NAME, running PROGRAM."
  (or switches
      (setq switches cafeobj-default-command-switches))
  (setq cafeobj-process-buffer (apply 'make-comint name program startfile switches))
  (setq cafeobj-process (get-buffer-process cafeobj-process-buffer))
  (save-excursion
    (set-buffer cafeobj-process-buffer)
    (cafeobj-process-mode)
    ))

(defun cafeobj (&rest ignore)
  (interactive)
  (if (and cafeobj-process
	   (eq (process-status cafeobj-process) 'run))
      (switch-to-buffer cafeobj-process-buffer)
      (progn
	(cafeobj-start-process cafeobj-default-application cafeobj-default-application)
	(switch-to-buffer-other-window cafeobj-process-buffer))))

(defun cafeobj2 (&rest ignore)
  (interactive)
  (if (and cafeobj-process
	   (eq (process-status cafeobj-process) 'run))
      (switch-to-buffer cafeobj-process-buffer)
      (progn
	(setq cafeobj-default-application "cafeobj2")
	(cafeobj-start-process cafeobj-default-application cafeobj-default-application)
	(switch-to-buffer-other-window cafeobj-process-buffer))))

(defun cafeobj-kill-process ()
  "Kill chaos subprocess and its buffer."
  (interactive)
  (if cafeobj-process-buffer
      (kill-buffer cafeobj-process-buffer)))

(defun cafeobj-set-cafeobj-region-start (&optional arg)
  "Set start of region for use with `cafeobj-send-cafeobj-region'."
  (interactive)
  (set-marker cafeobj-region-start (or arg (point))))

(defun cafeobj-set-cafeobj-region-end (&optional arg)
  "Set end of region for use with `cafeobj-send-cafeobj-region'."
  (interactive)
  (set-marker cafeobj-region-end (or arg (point))))

(defun cafeobj-send-line ()
  "Send current line to chaos subprocess, found in `cafeobj-process'.
If `cafeobj-process' is nil or dead, start a new process first."
  (interactive)
  (let ((start (save-excursion (beginning-of-line) (point)))
	(end (save-excursion (end-of-line) (point))))
    (or (and cafeobj-process
	     (eq (process-status cafeobj-process) 'run))
	(cafeobj-start-process cafeobj-default-application cafeobj-default-application))
    (comint-simple-send cafeobj-process (buffer-substring start end))
    (forward-line 1)
    (if cafeobj-always-show
	(display-buffer cafeobj-process-buffer))))

(defun cafeobj-send-region (start end)
  "Send region to chaos subprocess."
  (interactive "r")
  (or (and cafeobj-process
	   (comint-check-proc cafeobj-process-buffer))
      (cafeobj-start-process cafeobj-default-application cafeobj-default-application))
  (comint-simple-send cafeobj-process
		      (concat (buffer-substring start end) "\n"))
  (if cafeobj-always-show
      (display-buffer cafeobj-process-buffer)))

(defun cafeobj-send-decl ()
  "Send proc around point to chaos subprocess."
  (interactive)
  (let (beg end)
    (save-excursion
      (cafeobj-beginning-of-decl)
      (setq beg (point))
      (cafeobj-end-of-decl)
      (setq end (point)))
    (or (and cafeobj-process
	     (comint-check-proc cafeobj-process-buffer))
	(cafeobj-start-process cafeobj-default-application cafeobj-default-application))
    (comint-simple-send cafeobj-process
			(concat (buffer-substring beg end) "\n"))
    (if cafeobj-always-show
	(display-buffer cafeobj-process-buffer))))

(defun cafeobj-send-buffer ()
  "Send whole buffer to chaos subprocess."
  (interactive)
  (or (and cafeobj-process
	   (comint-check-proc cafeobj-process-buffer))
      (cafeobj-start-process cafeobj-default-application cafeobj-default-application))
  (if (buffer-modified-p)
      (comint-simple-send cafeobj-process
			  (concat
			   (buffer-substring (point-min) (point-max))
			   "\n"))
    (comint-simple-send cafeobj-process
			(concat "input "
				(buffer-file-name)
				"\n")))
  (if cafeobj-always-show
      (display-buffer cafeobj-process-buffer)))

(defun cafeobj-show-process-buffer ()
  "Make sure `cafeobj-process-buffer' is being displayed."
  (interactive)
  (display-buffer cafeobj-process-buffer))

(defun cafeobj-hide-process-buffer ()
  "Delete all windows that display `cafeobj-process-buffer'."
  (interactive)
  (delete-windows-on cafeobj-process-buffer))

;;; Set up the "Maude" pull-down menu
(easy-menu-define cafeobj-mode-menu cafeobj-mode-map
  "CafeOBJ mode menu."
  '("CafeOBJ"
    ["Evaluate buffer" cafeobj-send-buffer t]
    ["Evaluate region" cafeobj-send-region
     :active (if (boundp 'mark-active)
                 mark-active            ; emacs
               (region-exists-p)        ; xemacs
               )]
    ["Evaluate definition" cafeobj-send-definition t]
    ["---" nil nil]
    ["Run Maude" run-maude t]
    ["Switch to Maude" maude-switch-to-inferior-maude t]))


(provide 'cafeobj-mode)

;; Local Variables:
;; folded-file: t
;; End:

;;; cafeobj-mode.el ends here
